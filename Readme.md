# fsouza/fake-gcs-server 

Google Cloud Storage APIエミュレーターを使用して、本番に近い環境で開発を行えるようにする

## 使い方

storage/2019 にuploadしたいファイルを用意しておきます

```bash
docker-compose build
docker-compose up -d

# help
docker-compose run uploader
docker-compose run uploader -h

# ls
docker-compose run uploader ls 2019

# upload
docker-compose run uploader upload 2019

# remove
docker-compose run uploader rm 2019 -f file1.txt file2.txt file3.txt
```

* bucket直下にファイルをuploadすることはできません
* uploadは storage/nnnn の中身をまとめてuploadします
* rm で指定するファイル名はprefixなしです
* 完全に互換性があるわけではないようです。例えば更新日はないようで blob.updated としたところエラーになりました
* 本物のGoogle Cloud Storageにアクセスしたい場合はGOOGLE_APPLICATION_CREDENTIALSに指定したファイル名で認証情報のjsonファイルを置きます



## 参考
* [fsouza/fake-gcs-server](https://github.com/fsouza/fake-gcs-server)
* [Python Client for Google Cloud Storage](https://googleapis.dev/python/storage/latest/index.html)