import os
import argparse
import requests
import urllib3
from google.cloud import storage
from google.api_core.client_options import ClientOptions
from google.auth.credentials import AnonymousCredentials

input_dir = '../storage/gcs_input/'
gcs_container_name = os.getenv('GCS_CONTAINER_NAME')
gcs_bucket_name = os.getenv('GCS_BUCKET_NAME')


def get_blob_filename(filename):
    return "%s/%s" % (args.prefix, filename)

def set_args():
    parser = argparse.ArgumentParser(description="This script is gsc uploader.")
    parser.add_argument('service', choices=["upload", "rm", "ls"])
    parser.add_argument('prefix', help="(int)target dir prefix")
    parser.add_argument('--files', '-f', nargs="*", help="target files")
    return parser.parse_args()


def upload():
    for filename in os.listdir("../storage/%s" % args.prefix):
        if filename.startswith('.'):
            continue

        print('upload %s' % filename)

        blob = gcs_bucket.blob(get_blob_filename(filename))

        upload_filename = "../storage/%s" % (get_blob_filename(filename))
        blob.upload_from_filename(upload_filename)


def remove():
    for filename in args.files:
        print('deleted %s' % filename)
        blob = gcs_bucket.blob(get_blob_filename(filename))
        blob.delete()


def ls():
    for blob in gcs_bucket.list_blobs(prefix=args.prefix):
        print("Blob: %s" % blob.name)

def get_storage_client():
    if os.path.exists(os.getenv('GOOGLE_APPLICATION_CREDENTIALS', None)):
        return storage.Client()

    EXTERNAL_URL = os.getenv("EXTERNAL_URL", "https://%s:4443" % gcs_container_name)
    PUBLIC_HOST = os.getenv("PUBLIC_HOST", "storage.gcs.%s.nip.io:4443" % gcs_container_name)

    storage.blob._API_ACCESS_ENDPOINT = "https://" + PUBLIC_HOST
    storage.blob._DOWNLOAD_URL_TEMPLATE = (
        u"%s/download/storage/v1{path}?alt=media" % EXTERNAL_URL
    )
    storage.blob._BASE_UPLOAD_TEMPLATE = (
        u"%s/upload/storage/v1{bucket_path}/o?uploadType=" % EXTERNAL_URL
    )
    storage.blob._MULTIPART_URL_TEMPLATE = storage.blob._BASE_UPLOAD_TEMPLATE + u"multipart"
    storage.blob._RESUMABLE_URL_TEMPLATE = storage.blob._BASE_UPLOAD_TEMPLATE + u"resumable"

    my_http = requests.Session()
    my_http.verify = False  # disable SSL validation
    urllib3.disable_warnings(
        urllib3.exceptions.InsecureRequestWarning
    )  # disable https warnings for https insecure certs

    gcs_client = storage.Client(
        credentials=AnonymousCredentials(),
        project="test",
        _http=my_http,
        client_options=ClientOptions(api_endpoint=EXTERNAL_URL),
    )
    return gcs_client


if __name__ == "__main__":
    args = set_args()

    gcs_client = get_storage_client()
    # todo 不在の場合のみにしたい
    gcs_bucket = gcs_client.create_bucket(bucket_or_name=gcs_bucket_name)
    gcs_bucket = gcs_client.get_bucket(gcs_bucket_name)

    if args.service == 'upload':
        upload()
    elif args.service == 'rm':
        remove()
    elif args.service == 'ls':
        ls()

